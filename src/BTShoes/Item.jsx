import React from "react";

const Item = (props) => {
  const { item, handlePrdDetail } = props;
  return (
    <div className="col-4 mt-3">
      <div className="card">
        <img src={item.image} alt="..." />
        <div className="card-body">
          <p className="font-weight-bold">{item.name}</p>
          <p className="mt-3">{item.price} $</p>
          <button
            type="button"
            class="btn btn-outline-primary"
            data-toggle="modal"
            data-target="#exampleModal"
            onClick={() => { handlePrdDetail(item) }}
          >
            Xem chi tiết
          </button>
        </div>
      </div>
    </div>
  );
};

export default Item;
