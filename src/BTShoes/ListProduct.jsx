import React from "react";
import Item from "./Item";

const ListProduct = (props) => {
  const { data, handlePrdDetail } = props;
  return (
    <div className="row">
      {data.map((item) => (<Item item ={item} key={item.id} handlePrdDetail={handlePrdDetail}/>)
      )}
    </div>
  );
};

export default ListProduct;
